var express = require('express');
var router = express.Router();
const Dokter = require("../model/Dokter");
const { verify_token } = require('../middleware/verifytoken');


router.get("/", verify_token, async(req, res) => {
    try {
        const rows = await Dokter.findAll();

        if (rows.length == 0) {
            return res.status(404).send({ status: 404, message: "Data Tidak ditemukan" });
        } else {
            return res.json({ status: 200, message: "Data berhasil ditemukan", data: rows });
        }

    } catch (err) {
        console.error(err.message);
        res.status(500).send({ status: 500, message: "Gagal" })
    }
})

router.post("/insert", verify_token, async(req, res) => {
    try {
        const { nama, alamat } = req.body;

        const insert = await Dokter.create({ nama, alamat });
        return res.json({ status: 200, message: "Berhasil", data: insert });
    } catch (err) {
        console.error(err.message);
        res.status(500).send({ status: 500, message: "Gagal" })
    }
})

router.get("/detail/:id", verify_token, async(req, res) => {
    try {
        const id = req.params.id
        const detail = await Dokter.findOne({ where: { id: id } });

        if (detail == null) {
            return res.status(404).send({ status: 404, message: "Data Tidak ditemukan" });
        }
        return res.json({ status: 200, message: "Data berhasil ditemukan", data: detail });
    } catch (err) {
        console.error(err.message);
        res.status(500).send({ status: 500, message: "Gagal" });
    }
})

router.put("/update/:id", verify_token, async(req, res) => {
    try {
        const { nama, alamat } = req.body
        const id = req.params.id
        const find = await Dokter.findOne({ where: { id: id } });

        if (find == null) {
            return res.status(404).send({ status: 404, message: "Data Tidak ditemukan" })
        }

        await Dokter.update({ nama, alamat }, { where: { id: id } });

        return res.send({ status: 200, message: "Data berhasil diupdate" });
    } catch (err) {
        console.error(err.message);
        res.send({ status: 500, message: "Gagal Update" });
    }
})

router.put("/delete/:id", verify_token, async(req, res) => {
    try {
        const id = req.params.id
        const find = await Dokter.findOne({ where: { id: id } });

        if (find == null) {
            return res.status(404).send({ status: 404, message: "Data Tidak ditemukan" })
        }

        await Dokter.destroy({ where: { id: id } })
        return res.json({ status: 200, message: "Data berhasil dihapus" })
    } catch (err) {
        console.error(err.message);
        res.status(500).send({ status: 500, message: "Gagal" })
    }
})

module.exports = router;