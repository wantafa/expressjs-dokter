var express = require('express');
var router = express.Router();
const Dokter = require("../model/Dokter");
const Jadwal = require("../model/Jadwal");
const { verify_token } = require('../middleware/verifytoken');

const db = require("../config/db");

const { QueryTypes } = require("sequelize");


router.get("/", verify_token, async(req, res) => {
    try {
        const rows = await Jadwal.findAll();

        if (rows.length == 0) {
            return res.status(404).send({ status: 404, message: "Data Tidak ditemukan" });
        } else {
            return res.json({ status: 200, message: "Data berhasil ditemukan", data: rows });
        }

    } catch (err) {
        console.error(err.message);
        res.status(500).send({ status: 500, message: "Gagal" })
    }
})


router.get("/day/:day/:start/:end", verify_token, async(req, res) => {
    try {
        const day = req.params.day;
        const day_start = req.params.start;
        const day_end = req.params.end;

        const queryString = `
        SELECT 
            jd.doctor_id,
            jd.day,
            jd.time_start,
            jd.time_finish,
            jd.quota,
            jd.status,
            d.nama AS doctor_name,
            jd.date
        FROM 
            jadwal_dokter jd
        JOIN 
            dokter d ON jd.doctor_id = d.id
        WHERE 
            jd.day = :day AND
            jd.date BETWEEN :day_start AND :day_end
    `;

        const rows = await db.query(queryString, {
            replacements: {
                day: day,
                day_start: day_start,
                day_end: day_end,
            },
            type: QueryTypes.SELECT,
        });

        if (rows.length === 0) {
            return res.status(404).send({ status: 404, message: "Data Tidak ditemukan" });
        } else {
            const format_data = rows.map((row) => ({
                doctor_id: row.doctor_id,
                day: row.day,
                time_start: row.time_start,
                time_finish: row.time_finish,
                quota: row.quota,
                status: row.status,
                doctor_name: row.doctor_name,
                date: row.date,
            }));

            return res.json({ message: "Berhasil", data: format_data });
        }
    } catch (err) {
        console.error(err.message);
        res.status(500).send({ status: 500, message: "Gagal" });
    }
});

router.post("/insert", verify_token, async(req, res) => {
    try {
        const { doctor_id, day, time_start, time_finish, quota, status, doctor_name, date } = req.body;

        const insert = await Jadwal.create({ doctor_id, day, time_start, time_finish, quota, status, doctor_name, date });
        return res.json({ status: 200, message: "Berhasil", data: insert });
    } catch (err) {
        console.error(err.message);
        res.status(500).send({ status: 500, message: "Gagal" })
    }
})

router.get("/detail/:id", verify_token, async(req, res) => {
    try {
        const id = req.params.id
        const detail = await Jadwal.findOne({ where: { id: id } });

        if (detail == null) {
            return res.status(404).send({ status: 404, message: "Data Tidak ditemukan" });
        }
        return res.json({ status: 200, message: "Data berhasil ditemukan", data: detail });
    } catch (err) {
        console.error(err.message);
        res.status(500).send({ status: 500, message: "Gagal" });
    }
})

router.put("/update/:id", verify_token, async(req, res) => {
    try {
        const { doctor_id, day, time_start, time_finish, quota, status, doctor_name, date } = req.body
        const id = req.params.id
        const find = await Jadwal.findOne({ where: { id: id } });

        if (find == null) {
            return res.status(404).send({ status: 404, message: "Data Tidak ditemukan" })
        }

        await Jadwal.update({ doctor_id, day, time_start, time_finish, quota, status, doctor_name, date }, { where: { id: id } });

        return res.send({ status: 200, message: "Data berhasil diupdate" });
    } catch (err) {
        console.error(err.message);
        res.send({ status: 500, message: "Gagal Update" });
    }
})

router.put("/delete/:id", verify_token, async(req, res) => {
    try {
        const id = req.params.id
        const find = await Jadwal.findOne({ where: { id: id } });

        if (find == null) {
            return res.status(404).send({ status: 404, message: "Data Tidak ditemukan" })
        }

        await Jadwal.destroy({ where: { id: id } })
        return res.json({ status: 200, message: "Data berhasil dihapus" })
    } catch (err) {
        console.error(err.message);
        res.status(500).send({ status: 500, message: "Gagal" })
    }
})

module.exports = router;