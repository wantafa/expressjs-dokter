const Sequelize = require("sequelize");
const db = require("../config/db");
const Jadwal = require("./Jadwal");

const Dokter = db.define("dokter", {
    nama: { type: Sequelize.STRING },
    alamat: { type: Sequelize.STRING },
}, {
    freezeTableName: true
});

module.exports = Dokter;