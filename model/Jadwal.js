const Sequelize = require("sequelize");
const db = require("../config/db");
const Dokter = require("./Dokter");

const Jadwal = db.define("jadwal_dokter", {
    doctor_id: {
        type: Sequelize.INTEGER
    },
    day: { type: Sequelize.STRING },
    time_start: { type: Sequelize.TIME },
    time_finish: { type: Sequelize.TIME },
    quota: { type: Sequelize.INTEGER },
    status: { type: Sequelize.TINYINT },
    date: { type: Sequelize.DATE },
}, {
    freezeTableName: true
});

module.exports = Jadwal;