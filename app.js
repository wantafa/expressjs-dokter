var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const db = require("./config/db");
db.authenticate().then(() => console.log("Koneksi Berhasil"));

const { buat_token } = require('./middleware/verifytoken');

var DokterController = require('./controller/dokter');
var JadwalController = require('./controller/jadwal_dokter');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));

// app.use('/', (req, res) => console.log("respon berhasil"));
app.get('/login', buat_token);
app.use('/dokter', DokterController);
app.use('/jadwal_dokter', JadwalController);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;