const jwt = require('jsonwebtoken');
const User = require('../model/User');
const bcrypt = require('bcrypt');
const secret_key = 'jancoker';

// Middleware untuk membuat token
async function buat_token(req, res, next) {
    try {
        const { email, password } = req.body;

        const user = await User.findOne({ email });

        console.log();
        if (!user) {
            return res.status(404).json({ message: 'User tidak ditemukan' });
        }
        const isPasswordValid = await bcrypt.compare(password, user.password);
        if (!isPasswordValid) { return res.status(401).json({ message: 'Invalid password' }) };

        // Jika email dan kata sandi valid, buat dan kirim token
        const expires = 60 * 60 * 1;
        const token = jwt.sign({ email: user.email, alamat: user.email }, secret_key, { expiresIn: expires });

        res.json({ token, user });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal Server Error' });
    }
}

// Middleware untuk verifikasi token
function verify_token(req, res, next) {
    const token = req.headers.authorization;
    if (!token) {
        return res.status(401).json({ message: 'Token diperlukan' });
    }
    try {
        const token_req = token.split(' ')[1];
        const decoded = jwt.verify(token_req, secret_key);
        // console.log('Token valid:', decoded);
        req.user = decoded;
        next();
    } catch (error) {
        console.error('Token tidak valid:', error.message);
    }

    // jwt.verify(token, secret_key, (err, decoded) => {
    //     if (err) {
    //         return res.status(403).json({ message: 'Gagal' });
    //     }
    //     req.user = decoded;
    //     next();
    // });
}

module.exports = { buat_token, verify_token };