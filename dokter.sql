-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 30, 2023 at 02:02 PM
-- Server version: 5.7.24
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dokter`
--

-- --------------------------------------------------------

--
-- Table structure for table `dokter`
--

CREATE TABLE `dokter` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dokter`
--

INSERT INTO `dokter` (`id`, `nama`, `alamat`, `createdAt`, `updatedAt`) VALUES
(1, 'Dzakwan', 'Semarang', '2023-12-29 00:02:42', '2023-12-29 00:05:53'),
(2, 'Tirta', 'Jakarta', '2023-12-30 13:11:37', '2023-12-30 13:11:37');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_dokter`
--

CREATE TABLE `jadwal_dokter` (
  `id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `day` varchar(255) DEFAULT NULL,
  `time_start` time DEFAULT NULL,
  `time_finish` time DEFAULT NULL,
  `quota` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal_dokter`
--

INSERT INTO `jadwal_dokter` (`id`, `doctor_id`, `day`, `time_start`, `time_finish`, `quota`, `status`, `date`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'monday', '00:13:00', '00:14:00', 5, 1, '2023-01-16 00:00:00', '2023-12-29 00:42:43', '2023-12-29 00:42:43'),
(2, 2, 'tuesday', '08:00:00', '12:00:00', 6, 1, '2023-01-17 00:00:00', '2023-12-29 00:46:38', '2023-12-29 00:46:38'),
(3, 2, 'wednesday', '09:00:00', '16:00:00', 9, 1, '2023-01-18 00:00:00', '2023-12-29 00:49:23', '2023-12-29 00:49:23'),
(4, 1, 'monday', '13:00:00', '14:00:00', 9, 1, '2023-01-23 00:00:00', '2023-12-29 00:50:51', '2023-12-29 00:50:51'),
(5, 1, 'monday', '13:00:00', '14:00:00', 9, 1, '2023-01-30 00:00:00', '2023-12-29 00:51:49', '2023-12-29 00:51:49'),
(6, 1, 'tuesday', '08:00:00', '12:00:00', 9, 1, '2023-01-24 00:00:00', '2023-12-29 00:53:16', '2023-12-29 00:53:16');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `email`, `password`, `createdAt`, `updatedAt`) VALUES
(1, 'Dzakwan', 'admin@admin.com', '$2a$12$ArfAx/6YMgaLPh30q3fyT.MLCrV7HFB6ASDOWeDopavRsAT8KLROe', '2023-12-29 00:39:06', '2023-12-29 00:39:06'),
(2, 'Admin', 'subadmin@admin.com', '$2a$12$ArfAx/6YMgaLPh30q3fyT.MLCrV7HFB6ASDOWeDopavRsAT8KLROe', '2023-12-29 00:39:53', '2023-12-29 00:39:53');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dokter`
--
ALTER TABLE `dokter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jadwal_dokter`
--
ALTER TABLE `jadwal_dokter`
  ADD PRIMARY KEY (`id`),
  ADD KEY `doctor_id` (`doctor_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dokter`
--
ALTER TABLE `dokter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jadwal_dokter`
--
ALTER TABLE `jadwal_dokter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jadwal_dokter`
--
ALTER TABLE `jadwal_dokter`
  ADD CONSTRAINT `jadwal_dokter_ibfk_1` FOREIGN KEY (`doctor_id`) REFERENCES `dokter` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
